#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include <stdio.h>
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
#include <GLFW/glfw3.h> // Will drag system OpenGL headers

#include <iostream>
#include "cuda renderer/camera.h"
#include "cuda renderer/hitable.h"
#include "Renderer.h"
#include <time.h>
#include "vec2.h"
#include <string>
#include <vector>


#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

#define WIDTH 150
#define HEIGHT 100
#define PI 3.1415
#define EXPOSURE 1
#define GAMMA 2.2
#define STEP 0.2f
#define GUIFRAMES 5
#define SCALE 6

//vec3 viewPoint(-5.0f, 0.0f, 0.0f);
vec3* viewPoint;
vec3* target;

static void ShowAppMainMenuBar(bool& showAddShape, bool& showStatistics);
static void ShowFileMenu();
static void ShowEditMenu();
static void ShowViewMenu(bool& showAddShape, bool& showStatistics);
static void ShowStatistics(bool* p_open, double fps);
static void ShowAddShape(bool* p_open, hitable_list& scene);
double clockToMilliseconds(clock_t ticks);
void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
static void ShowImage(bool* p_open, GLuint* imageTexture, int imageWidth, int imageHeight);
void rotate(float angleDelta);
void rotate(vec2 angleDelta);
vec3 calcTarget(vec3 view, float angle);

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

int main(int, char**)
{
    // Setup window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    // Decide GL+GLSL versions
#if defined(IMGUI_IMPL_OPENGL_ES2)
    // GL ES 2.0 + GLSL 100
    const char* glsl_version = "#version 100";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
#elif defined(__APPLE__)
    // GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1280, 720, "Ray Tracing Engine GUI prototype", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
    glfwSetKeyCallback(window, glfwKeyCallback);

    // Our state
    bool showAddShape = true;
    bool showStatistics = false;
    bool showRenderedImage = true;
    ImVec4 clear_color = ImVec4(0.34f, 0.34f, 0.34f, 1.00f);

    /*int my_image_width = 0;
    int my_image_height = 0;
    GLuint my_image_texture = 0;
    bool ret = LoadRenderedImageToTexture("myimage.jpg", &my_image_texture, &my_image_width, &my_image_height);
    IM_ASSERT(ret);*/
    int imageWidth = WIDTH;
    int imageHeight = HEIGHT;
    GLuint* imageTexture;



    clock_t deltaTime = 0;
    unsigned int frames = 0;
    double  frameRate = 30;
    double  averageFrameTimeMilliseconds = 33.333;

    Renderer renderer(imageWidth, imageHeight, vec3(-2.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, 0.0f), vec3(), PI / 4, (float)imageWidth / (float)imageHeight);
    imageTexture = renderer.getTexture();
    viewPoint = renderer.getViewPoint();
    target = renderer.getTarget();
    hitable_list& scene = renderer.getScene();
    // Main loop
    int count = GUIFRAMES; //temp solution for slow frame time until cuda
    while (!glfwWindowShouldClose(window))
    {
        clock_t beginFrame = clock();
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        ShowAppMainMenuBar(showAddShape, showStatistics);
        if (showAddShape) ShowAddShape(&showAddShape, scene);
        if (showStatistics) ShowStatistics(&showStatistics, frameRate);

        if (count == GUIFRAMES) //temp solution for slow frame time until cuda
        {
            renderer.reRenderToTexture();
            count = 0;
        }
        else count++;
        ShowImage(&showRenderedImage, imageTexture, imageWidth, imageHeight);

        if (ImGui::IsMouseDragging(0))
        {
            auto a = ImGui::GetMouseDragDelta();
            rotate(vec2(0.2f * a.x, -0.2f * a.y));
            ImGui::ResetMouseDragDelta();
        }

        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);

        clock_t endFrame = clock();

        deltaTime += endFrame - beginFrame;
        frames++;

        if (clockToMilliseconds(deltaTime) > 1000.0) { //every second
            frameRate = (double)frames * 0.5 + frameRate * 0.5; //more stable
            frames = 0;
            deltaTime -= CLOCKS_PER_SEC;
            averageFrameTimeMilliseconds = 1000.0 / (frameRate == 0 ? 0.001 : frameRate);
            //std::cout << averageFrameTimeMilliseconds << std::endl;
        }
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}

static void ShowImage(bool* p_open, GLuint* my_image_texture, int my_image_width, int my_image_height)
{
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoBackground;
    ImGui::Begin("OpenGL Texture Text", p_open, window_flags);
    ImGui::Image((void*)(intptr_t)*my_image_texture, ImVec2(my_image_width * SCALE, my_image_height * SCALE));
    ImGui::Text((std::to_string((*viewPoint).x()) + ", " + std::to_string((*viewPoint).y()) + ", " + std::to_string((*viewPoint).z())).c_str());
    //ImGui::Text("vec3er = %p", *my_image_texture);
    ImGui::End();
    //delete[](void*)(intptr_t)my_image_texture;
}


static void ShowAppMainMenuBar(bool& showAddShape, bool& showStatistics)
{
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            ShowFileMenu();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Edit"))
        {
            ShowEditMenu();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("View"))
        {
            ShowViewMenu(showAddShape, showStatistics);
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

static void ShowFileMenu()
{
    if (ImGui::MenuItem("New")) {}
    if (ImGui::MenuItem("Open", "Ctrl+O")) {}
    if (ImGui::MenuItem("Save", "Ctrl+S")) {}
    if (ImGui::MenuItem("Save As..")) {}
}

static void ShowEditMenu()
{
    if (ImGui::MenuItem("Cut", "CTRL+X")) {}
    if (ImGui::MenuItem("Copy", "CTRL+C")) {}
    if (ImGui::MenuItem("Paste", "CTRL+V")) {}
}

static void ShowViewMenu(bool& showAddShape, bool& showStatistics)
{
    if (ImGui::MenuItem("Add Shapes")) { showAddShape = !showAddShape; }
    if (ImGui::MenuItem("Statistics")) { showStatistics = !showStatistics; }
}

static void ShowAddShape(bool* p_open, hitable_list& scene)
{
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    static vec3 color(0.45f, 0.55f, 0.66f);
    if (!ImGui::Begin("Shapes", p_open, window_flags))
    {
        ImGui::End();
        return;
    }
    //static int selectedShape = 0;
    //ImGui::Combo("Select Shape", &selectedShape, "Sphere\0Plane\0Triangle\0");
    //Material* mat = new Diffuse();
    //switch (selectedShape)
    //{
    //case 0:
    //{
    //    static vec3 vec3(0.0f, 0.0f, 0.0f);
    //    ImGui::InputFloat3("sphere center", (float*)&vec3);
    //    static float radius = 0.5f;
    //    ImGui::InputFloat("radius", &radius, 0.5f, 2.0f, "%.3f");

    //    if (ImGui::Button("Create"))
    //    {
    //        Sphere* sphere = new Sphere(vec3, radius, color, mat);
    //        scene.addShape(sphere);
    //    }
    //    break;
    //}
    //case 1:
    //{
    //    static vec3 vec3(0.0f, 0.0f, 0.0f);
    //    ImGui::InputFloat3("vec3", (float*)&vec3);
    //    static vec3 normal(0.0f, 0.1f, 0.0f);
    //    ImGui::InputFloat3("normal vector", (float*)&normal);

    //    if (ImGui::Button("Create"))
    //    {
    //        Plane* plane = new Plane(vec3, normal, color, mat);
    //        scene.addShape(plane);
    //    }
    //    break;
    //}
    //case 2:
    //{
    //    static vec3 vec31(0.0f, 0.0f, 0.0f);
    //    ImGui::InputFloat3("Vertex 1", (float*)&vec31);
    //    static vec3 vec32(0.0f, 0.0f, 0.0f);
    //    ImGui::InputFloat3("Vertex 2", (float*)&vec32);
    //    static vec3 vec33(0.0f, 0.0f, 0.0f);
    //    ImGui::InputFloat3("Vertex 3", (float*)&vec33);

    //    if (ImGui::Button("Create"))
    //    {
    //        Triangle* triangle = new Triangle(vec31, vec32, vec33, color);
    //        scene.addShape(triangle);
    //    }
    //    break;
    //}
    //default:
    //    break;
    //}
    ////ImGuiColorEditFlags flags;
    //ImGui::ColorPicker4("Shape Color", (float*)&color);

    ImGui::End();
}

static void ShowStatistics(bool* p_open, double fps)
{
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoResize;
    if (!ImGui::Begin("Statistics", p_open, window_flags))
    {
        ImGui::End();
        return;
    }
    ImGui::Text(std::string(std::to_string(int(fps)) + " fps").c_str());
    static std::vector<float> values;
    static int count = 0;
    if (count == 0)
    {
        values.push_back(float(fps));
        count = 5;
    }
    else
    {
        count--;
    }
    float* valuesPtr = &values[0];
    ImGui::PlotLines("", valuesPtr, values.size());
    ImGui::End();
}



double clockToMilliseconds(clock_t ticks) {
    // units/(units/time) => time (seconds) * 1000 = milliseconds
    return (ticks / (double)CLOCKS_PER_SEC) * 1000.0;
}

vec3 calcTarget(vec3 view, float angle)
{
    if (angle == 90) return (view + vec3(0, 0, -1));
    if (angle == 270) return (view + vec3(0, 0, 1));

    return view + vec3(((angle < 90 || angle > 270) ? 1 : -1), 0, ((angle < 90 || angle > 270) ? -1 : 1) * tan(angle * PI / 180));
}

vec3 calcTarget(vec3 view, vec2 angle)
{
    if (angle.u == 90) return (view + vec3(0, tan(angle.v * PI / 180), -1));
    if (angle.u == 270) return (view + vec3(0, tan(angle.v * PI / 180), 1));

    return view + vec3(((angle.u < 90 || angle.u > 270) ? 1 : -1), tan(angle.v * PI / 180), ((angle.u < 90 || angle.u > 270) ? -1 : 1) * tan(angle.u * PI / 180));
}

void rotate(float angleDelta)
{
    static float angle = 0;

    angle = fmod((angle + 360 + angleDelta), 360);
    (*target) = calcTarget(*viewPoint, angle);
}

void rotate(vec2 angleDelta)
{
    static vec2 angle = 0;

    angle = vec2(fmod((angle.u + 360 + angleDelta.u), 360), fmod((angle.v + 180 + angleDelta.v), 180));
    (*target) = calcTarget(*viewPoint, angle);
}

void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_W && action == GLFW_PRESS)
    {
        (*viewPoint) += STEP * ((*target - *viewPoint).normalized());
        (*target) += STEP * ((*target - *viewPoint).normalized());
    }
    else if (key == GLFW_KEY_A && action == GLFW_PRESS)
    {
        vec3 directionStep(STEP * ((*target - *viewPoint).normalized()));

        (*viewPoint).e[0] += directionStep.z();
        (*viewPoint).e[2] -= directionStep.x();
        (*target).e[0] += directionStep.z();
        (*target).e[2] -= directionStep.x();
    }
    else if (key == GLFW_KEY_S && action == GLFW_PRESS)
    {
        (*viewPoint) -= STEP * ((*target - *viewPoint).normalized());
        (*target) -= STEP * ((*target - *viewPoint).normalized());
    }
    else if (key == GLFW_KEY_D && action == GLFW_PRESS)
    {
        vec3 directionStep(STEP * ((*target - *viewPoint).normalized()));

        (*viewPoint).e[0] -= directionStep.z();
        (*viewPoint).e[2] += directionStep.x();
        (*target).e[0] -= directionStep.z();
        (*target).e[2] += directionStep.x();
    }
    else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
    {
        *viewPoint -= vec3(0, STEP, 0);
        *target -= vec3(0, STEP, 0);
    }
    else if (key == GLFW_KEY_LEFT_CONTROL && action == GLFW_PRESS)
    {
        *viewPoint += vec3(0, STEP, 0);
        *target += vec3(0, STEP, 0);
    }
    else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
    {
        rotate(vec2(-30, 0));
    }
    else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
    {
        rotate(vec2(30, 0));
    }
}