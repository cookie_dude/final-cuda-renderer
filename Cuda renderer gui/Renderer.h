#pragma once
#include "cuda renderer/camera.h"
#include "cuda renderer/vec3.h"
#include "cuda renderer/ray.h"
#include "cuda renderer/sphere.h"
#include "cuda renderer/hitable_list.h"
#include "cuda renderer/camera.h"
#include "cuda renderer/material.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <float.h>
#include <curand_kernel.h>
#include <Windows.h>
#include "cudaRender.h"

#define SAMPLES_PER_PIXEL 6 // should be like 100


class Renderer
{
private:
	vec3 *_image;
	camera _camera;
	hitable_list _scene;
	GLuint _texture;
	vec3 _viewPoint;
	vec3 _target;
	vec3 _upGuide;
	float _fov;
	float _aspectRatio;
	int _width;
	int _height;

public:
	Renderer(int width, int height, vec3 viewPoint, vec3 target, vec3 upGuide, float fov, float aspectRatio);
	void reRenderToTexture();
	void rayTrace();
	GLuint* getTexture();
	vec3* getViewPoint();
	vec3* getTarget();
	hitable_list& getScene();
	//static std::vector<Triangle*> loadOBJ();
};

