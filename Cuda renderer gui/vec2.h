#pragma once
struct vec2 {
	float u, v;

	vec2();
	vec2(const vec2& v);
	vec2(float u, float v);
	vec2(float f);

	virtual ~vec2();

	vec2& operator =(const vec2& v);
};

vec2::vec2() : u(0.0f), v(0.0f) {}


vec2::vec2(const vec2& v) : u(v.u), v(v.v) {}


vec2::vec2(float u, float v) : u(u), v(v) {}


vec2::vec2(float f) : u(f), v(f) {}


vec2::~vec2() {

}


vec2& vec2::operator =(const vec2& v) {
	u = v.u;
	this->v = v.v;

	return *this;
}