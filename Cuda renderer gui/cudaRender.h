#pragma once
#include "cuda renderer/camera.h"
#include "cuda renderer/vec3.h"
#include "cuda renderer/ray.h"
#include "cuda renderer/sphere.h"
#include "cuda renderer/hitable_list.h"
#include "cuda renderer/camera.h"
#include "cuda renderer/material.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <float.h>
#include <curand_kernel.h>
#include <Windows.h>

void cudaRayTrace(int width, int height, vec3* image);