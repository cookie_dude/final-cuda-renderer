#ifndef TRIANGLEH
#define TRIANGLEH

#include "hitable.h"

class triangle : public hitable {
public:
    __device__ triangle() {}
    __device__ triangle(vec3 vertex1, vec3 vertex2, vec3 vertex3, material* m) : _vertex1(vertex1), _vertex2(vertex2), _vertex3(vertex3), mat_ptr(m) {};
	__device__ ~triangle() { delete mat_ptr; };
    __device__ virtual bool hit(const ray& r, float tmin, float tmax, hit_record& rec) const;
    vec3 _vertex1, _vertex2, _vertex3;
    material* mat_ptr;
};


inline __device__ bool triangle::hit(const ray& r, float t_min, float t_max, hit_record& rec) const 
{
	const float EPSILON = 0.0001;
	vec3 edge1, edge2, h, s, q;
	float a, f, u, v;
	edge1 = _vertex2 - _vertex1;
	edge2 = _vertex3 - _vertex1;
	h = cross(r.direction(), edge2);
	a = dot(edge1, h);
	if (a > -EPSILON && a < EPSILON)
		return false;    // This ray is parallel to this triangle.
	f = 1.0 / a;
	s = r.origin() - _vertex1;
	u = f * dot(s, h);
	if (u < 0.0 || u > 1.0)
		return false;
	q = cross(s, edge1);
	v = f * dot(r.direction(), q);
	if (v < 0.0 || u + v > 1.0)
		return false;
	// At this stage we can compute t to find out where the intersection point is on the line.
	float t = f * dot(edge2, q);
	if (t > t_min && t < t_max) // ray intersection
	{
        rec.t = t;
        rec.p = r.point_at_parameter(rec.t);
		vec3 normal = cross(edge1, edge2);
		float cosA = dot(normal, r.direction()) / r.direction().length();
		//printf("%d/n", r.direction().length());
		rec.normal = (cosA > 0) ? normal : -normal;
        rec.mat_ptr = mat_ptr;
        return true;
	}
	else // This means that there is a line intersection but not a ray intersection.
		return false;
}

#endif
