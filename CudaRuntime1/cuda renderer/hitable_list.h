#ifndef HITABLELISTH
#define HITABLELISTH

#include "hitable.h"
#define MAXSHAPES 2500

#define checkCudaErrors(val) check_cuda( (val), #val, __FILE__, __LINE__ )

inline void check_cuda(cudaError_t result, char const* const func, const char* const file, int const line) {
    if (result) {
        std::cerr << "CUDA error = " << static_cast<unsigned int>(result) << " at " <<
            file << ":" << line << " '" << func << "' \n";
        // Make sure we call CUDA Device Reset before exiting
        cudaDeviceReset();
        exit(99);
    }
}

class hitable_list: public hitable  {
    public:
        __device__ hitable_list() {}
        __device__ hitable_list(hitable **l, int n) {list = l; list_size = n; }
        __device__ virtual bool hit(const ray& r, float tmin, float tmax, hit_record& rec) const;
        __device__ void hitable_list::addShape(hitable* newHitable);
        hitable **list;
        int list_size;
};

inline __device__ bool hitable_list::hit(const ray& r, float t_min, float t_max, hit_record& rec) const {
        hit_record temp_rec;
        bool hit_anything = false;
        float closest_so_far = t_max;
        for (int i = 0; i < list_size; i++) {
            if (list[i]->hit(r, t_min, closest_so_far, temp_rec)) {
                hit_anything = true;
                closest_so_far = temp_rec.t;
                rec = temp_rec;
            }
        }
        return hit_anything;
}

inline __device__ void hitable_list::addShape(hitable* newHitable)
{
    /*hitable** l;
    cudaMalloc((void**)&l, (list_size+1) * sizeof(hitable*));
    for (int i = 0; i < list_size; i++)
    {
        l[i] = list[i];
    }
    delete* list;
    l[list_size] = newHitable;
    list_size++;
    list = l;
    printf("aaaa %d\n", list_size);*/
    if (list_size == MAXSHAPES)
    {
        printf("ERROR: maximum amount of shapes reached");
        delete newHitable;
    }
    else
    {
        list[list_size] = newHitable;
        list_size++;
        printf("%d\n", list_size);
    }
}

#endif


