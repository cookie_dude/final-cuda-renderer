#include "cuda renderer/camera.h"
#include "cuda renderer/vec3.h"
#include "cuda renderer/ray.h"
#include "cuda renderer/sphere.h"
#include "cuda renderer/Triangle.h"
#include "cuda renderer/hitable_list.h"
#include "cuda renderer/camera.h"
#include "cuda renderer/material.h"
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <float.h>
#include <curand_kernel.h>
#include <Windows.h>


void cudaRayTrace(int width, int height, vec3 *image, camera** d_camera, hitable** d_world, curandState* d_rand_state);
void cudaCreateWorld(hitable** d_list, hitable** d_world, camera** d_camera, int width, int height, curandState* d_rand_state2);
void cudaClean(hitable** d_list, hitable** d_world, camera** d_camera, curandState* d_rand_state, curandState* d_rand_state2, vec3* image);
void cudaAddTransparentSphere(hitable** d_world, vec3 cen, float r, float ri);
void cudaAddMetalicSphere(hitable** d_world, vec3 cen, float r, vec3 color, float reflectiveness);
void cudaAddDiffuseSphere(hitable** d_world, vec3 cen, float r, vec3 color);
void cudaAddTransparentTriangle(hitable** d_world, vec3 vertex1, vec3 vertex2, vec3 vertex3, float ri);
void cudaAddMetalicTriangle(hitable** d_world, vec3 vertex1, vec3 vertex2, vec3 vertex3, vec3 color, float reflectiveness);
void cudaAddDiffuseTriangle(hitable** d_world, vec3 vertex1, vec3 vertex2, vec3 vertex3, vec3 color);
void cudaSetCamera(camera** d_camera, int width, int height, vec3 viewPoint, vec3 target);