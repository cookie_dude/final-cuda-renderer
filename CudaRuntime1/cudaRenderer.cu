#include <stdio.h>
#include "cudaRenderer.h"

__device__ vec3 color(const ray& r, hitable** world, curandState* local_rand_state) {
    ray cur_ray = r;
    vec3 cur_attenuation = vec3(1.0, 1.0, 1.0);
    for (int i = 0; i < 50; i++) {
        hit_record rec;
        if ((*world)->hit(cur_ray, 0.001f, FLT_MAX, rec)) {
            ray scattered;
            vec3 attenuation;
            if (rec.mat_ptr->scatter(cur_ray, rec, attenuation, scattered, local_rand_state)) {
                cur_attenuation *= attenuation;
                cur_ray = scattered;
            }
            else {
                return vec3(0.0, 0.0, 0.0);
            }
        }
        else {
            vec3 unit_direction = unit_vector(cur_ray.direction());
            float t = 0.5f * (unit_direction.y() + 1.0f);
            vec3 c = (1.0f - t) * vec3(1.0, 1.0, 1.0) + t * vec3(0.5, 0.7, 1.0);
            return cur_attenuation * c;
        }
    }
    return vec3(0.0, 0.0, 0.0); // exceeded recursion
}

__global__ void rand_init(curandState* rand_state) {
    if (threadIdx.x == 0 && blockIdx.x == 0) {
        curand_init(1984, 0, 0, rand_state);
    }
}

__global__ void render_init(int max_x, int max_y, curandState* rand_state) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    if ((i >= max_x) || (j >= max_y)) return;
    int pixel_index = j * max_x + i;
    // Original: Each thread gets same seed, a different sequence number, no offset
    // curand_init(1984, pixel_index, 0, &rand_state[pixel_index]);
    // BUGFIX, see Issue#2: Each thread gets different seed, same sequence for
    // performance improvement of about 2x!
    curand_init(1984 + pixel_index, 0, 0, &rand_state[pixel_index]);
}

__global__ void render(vec3* fb, int max_x, int max_y, int ns, camera** cam, hitable** world, curandState* rand_state) {
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    if ((i >= max_x) || (j >= max_y)) return;
    int pixel_index = j * max_x + i;
    curandState local_rand_state = rand_state[pixel_index];
    vec3 col(0, 0, 0);
    for (int s = 0; s < ns; s++) {
        float u = float(i + curand_uniform(&local_rand_state)) / float(max_x);
        float v = float(j + curand_uniform(&local_rand_state)) / float(max_y);
        ray r = (*cam)->get_ray(u, v, &local_rand_state);
        col += color(r, world, &local_rand_state);
    }
    rand_state[pixel_index] = local_rand_state;
    col /= float(ns);
    col[0] = sqrt(col[0]);
    col[1] = sqrt(col[1]);
    col[2] = sqrt(col[2]);
    fb[pixel_index] = col;
}

#define RND (curand_uniform(&local_rand_state))

__global__ void create_world(hitable** d_list, hitable** d_world, camera** d_camera, int nx, int ny, curandState* rand_state) {
    if (threadIdx.x == 0 && blockIdx.x == 0) {
        curandState local_rand_state = *rand_state;
        d_list[0] = new sphere(vec3(0, -1000.0, -1), 1000,
            new lambertian(vec3(0.5, 0.5, 0.5)));
        int i = 1;
        
        //d_list[i++] = new sphere(vec3(0, 1, 0), 1.0, new dielectric(1.5));
        d_list[i++] = new sphere(vec3(-4, 1, 0), 1.0, new lambertian(vec3(0.4, 0.2, 0.1)));
        d_list[i++] = new sphere(vec3(4, 1, 0), 1.0, new metal(vec3(0.7, 0.6, 0.5), 0.0));
        *rand_state = local_rand_state;
        *d_world = new hitable_list(d_list, 3);

        vec3 lookfrom(13, 2, 3);
        vec3 lookat(0, 0, 0);
        float dist_to_focus = 10.0; (lookfrom - lookat).length();
        float aperture = 0.1;
        *d_camera = new camera(lookfrom,
            lookat,
            vec3(0, 1, 0),
            30.0,
            float(nx) / float(ny),
            aperture,
            dist_to_focus);
    }
}

__global__ void setCamera(camera** d_camera, int width, int height, vec3 viewPoint, vec3 target)
{
    delete* d_camera;
    float dist_to_focus = 800.0f; (viewPoint - target).length();
    float aperture = 0.1;

    *d_camera = new camera(viewPoint,
        target,
        vec3(0, 1, 0),
        30.0,
        float(width) / float(height),
        aperture,
        dist_to_focus);
}

__global__ void free_world(hitable** d_list, hitable** d_world, camera** d_camera) {
    for (int i = 0; i < MAXSHAPES; i++) {
        delete ((sphere*)d_list[i])->mat_ptr;
        delete d_list[i];
    }
    delete* d_world;
    delete* d_camera;
}

//Add Sphere
__global__ void addTransparentSphere(hitable** d_world, vec3 cen, float r, float ri)
{
    hitable_list* world = (hitable_list*)*d_world;
    world->addShape( new sphere(cen, r, new dielectric(ri)));
}

__global__ void addMetalicSphere(hitable** d_world, vec3 cen, float r, vec3 color, float reflectiveness)
{
    hitable_list* world = (hitable_list*)*d_world;
    world->addShape(new sphere(cen, r, new metal(color, reflectiveness)));
}

__global__ void addDiffuseSphere(hitable** d_world, vec3 cen, float r, vec3 color)
{
    hitable_list* world = (hitable_list*)*d_world;
    world->addShape(new sphere(cen, r, new lambertian(color)));
}

//Add Triangle
__global__ void addTransparentTriangle(hitable** d_world, vec3 vertex1, vec3 vertex2, vec3 vertex3, float ri)
{
    hitable_list* world = (hitable_list*)*d_world;
    world->addShape(new triangle(vertex1, vertex2, vertex3, new dielectric(ri)));
}

__global__ void addMetalicTriangle(hitable** d_world, vec3 vertex1, vec3 vertex2, vec3 vertex3, vec3 color, float reflectiveness)
{
    hitable_list* world = (hitable_list*)*d_world;
    world->addShape(new triangle(vertex1, vertex2, vertex3, new metal(color, reflectiveness)));
}

__global__ void addDiffuseTriangle(hitable** d_world, vec3 vertex1, vec3 vertex2, vec3 vertex3, vec3 color)
{
    hitable_list* world = (hitable_list*)*d_world;
    world->addShape(new triangle(vertex1, vertex2, vertex3, new lambertian(color)));
}

void cudaRayTrace(int width, int height, vec3* image, camera** d_camera, hitable** d_world, curandState* d_rand_state)
{
    int num_pixels = width * height;
    int ns = 4;
    int tx = 8;
    int ty = 8;
    
    // Render buffer
    dim3 blocks(width / tx + 1, height / ty + 1);
    dim3 threads(tx, ty);
    render_init<<<blocks, threads>>>(width, height, d_rand_state);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
    render<<<blocks, threads>>>(image, width, height, ns, d_camera, d_world, d_rand_state);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
}

void cudaCreateWorld(hitable** d_list, hitable** d_world, camera** d_camera, int width, int height, curandState* d_rand_state2)
{
    rand_init<<<1, 1>>>(d_rand_state2);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
    create_world<<<1, 1>>>(d_list, d_world, d_camera, width, height, d_rand_state2);

}

void cudaClean(hitable** d_list, hitable** d_world, camera** d_camera, curandState* d_rand_state, curandState* d_rand_state2, vec3* image)
{
    // clean up
    checkCudaErrors(cudaDeviceSynchronize());
    free_world<<<1, 1>>>(d_list, d_world, d_camera);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaFree(d_camera));
    checkCudaErrors(cudaFree(d_world));
    checkCudaErrors(cudaFree(d_list));
    checkCudaErrors(cudaFree(d_rand_state));
    checkCudaErrors(cudaFree(d_rand_state2));
    checkCudaErrors(cudaFree(image));
    
    cudaDeviceReset();
}

void cudaAddTransparentSphere(hitable** d_world, vec3 cen, float r, float ri)
{
    addTransparentSphere<<<1, 1>>>(d_world, cen, r, ri);
}

void cudaAddMetalicSphere(hitable** d_world, vec3 cen, float r, vec3 color, float reflectiveness)
{
    addMetalicSphere<<<1, 1>>>(d_world, cen, r, color, reflectiveness);
}

void cudaAddDiffuseSphere(hitable** d_world, vec3 cen, float r, vec3 color)
{
    addDiffuseSphere<<<1, 1>>>(d_world, cen, r, color);
}

void cudaAddTransparentTriangle(hitable** d_world, vec3 vertex1, vec3 vertex2, vec3 vertex3, float ri)
{
    addTransparentTriangle<<<1, 1>>>(d_world, vertex1, vertex2, vertex3, ri);
}

void cudaAddMetalicTriangle(hitable** d_world, vec3 vertex1, vec3 vertex2, vec3 vertex3, vec3 color, float reflectiveness)
{
    addMetalicTriangle<<<1, 1>>>(d_world, vertex1, vertex2, vertex3, color, reflectiveness);
}

void cudaAddDiffuseTriangle(hitable** d_world, vec3 vertex1, vec3 vertex2, vec3 vertex3, vec3 color)
{
    addDiffuseTriangle<<<1, 1>>>(d_world, vertex1, vertex2, vertex3, color);
}

void cudaSetCamera(camera** d_camera, int width, int height, vec3 viewPoint, vec3 target)
{
    setCamera<<<1, 1>>>(d_camera, width, height, viewPoint, target);
}