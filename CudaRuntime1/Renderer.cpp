#include "Renderer.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"


Renderer::Renderer(int width, int height, vec3 viewPoint, vec3 target, vec3 upGuide, float fov, float aspectRatio)
    :_width(width), _height(height), _viewPoint(viewPoint), _target(target), _upGuide(upGuide), _fov(fov), _aspectRatio(aspectRatio)
{
    int num_pixels = width * height;
    checkCudaErrors(cudaMallocManaged((void**)&_image, width* height * sizeof(vec3)));
    checkCudaErrors(cudaMalloc((void**)&d_rand_state, num_pixels * sizeof(curandState)));
    checkCudaErrors(cudaMalloc((void**)&d_rand_state2, 1 * sizeof(curandState)));

    //make the scene
    checkCudaErrors(cudaMalloc((void**)&d_list, MAXSHAPES * sizeof(hitable*)));
    checkCudaErrors(cudaMalloc((void**)&d_world, sizeof(hitable*)));
    checkCudaErrors(cudaMalloc((void**)&d_camera, sizeof(camera*)));
    cudaCreateWorld(d_list, d_world, d_camera, _width, _height, d_rand_state2);
    //loadOBJ("tree low.obj");
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());

    rayTrace();

    // Create a OpenGL texture identifier
    GLuint image_texture;
    glGenTextures(1, &image_texture);
    glBindTexture(GL_TEXTURE_2D, image_texture);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // This is required on WebGL for non power-of-two textures
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Same

    // Upload pixels into texture
#if defined(GL_UNPACK_ROW_LENGTH) && !defined(__EMSCRIPTEN__)
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
#endif
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_FLOAT, (float*)_image);
    _texture = image_texture;
}

Renderer::~Renderer()
{
    cudaClean(d_list, d_world, d_camera, d_rand_state, d_rand_state2, _image);
}


void Renderer::reRenderToTexture()
{
    //d_camera = 
    rayTrace();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _width, _height, 0, GL_RGB, GL_FLOAT, _image);
}

void Renderer::rayTrace()
{
    cudaRayTrace(_width, _height, _image, d_camera, d_world, d_rand_state);
}

GLuint* Renderer::getTexture()
{
    return &_texture;
}

vec3* Renderer::getViewPoint()
{
    return &_viewPoint;
}

vec3* Renderer::getTarget()
{
    return &_target;
}

hitable** Renderer::getScene()
{
    return d_world;
}

camera** Renderer::getCamera()
{
    return d_camera;
}

void Renderer::loadOBJ(std::string path)
{
    tinyobj::ObjReaderConfig reader_config;
    reader_config.mtl_search_path = "./"; // Path to material files

    tinyobj::ObjReader reader;

    if (!reader.ParseFromFile(path, reader_config)) {
        if (!reader.Error().empty()) {
            std::cerr << "TinyObjReader: " << reader.Error();
        }
        exit(1);
    }

    if (!reader.Warning().empty()) {
        std::cout << "TinyObjReader: " << reader.Warning();
    }

    auto& attrib = reader.GetAttrib();
    auto& shapes = reader.GetShapes();
    auto& materials = reader.GetMaterials();
    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++) {
        // Loop over faces(polygon)
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
            size_t fv = size_t(shapes[s].mesh.num_face_vertices[f]);

            // Loop over vertices in the face.
            std::vector<vec3> vertices;
            for (size_t v = 0; v < fv; v++) {
                // access to vertex
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
                tinyobj::real_t vx = attrib.vertices[3 * size_t(idx.vertex_index) + 0];
                tinyobj::real_t vy = attrib.vertices[3 * size_t(idx.vertex_index) + 1];
                tinyobj::real_t vz = attrib.vertices[3 * size_t(idx.vertex_index) + 2];

                vertices.push_back(vec3(float(vx), float(vy)+1, float(vz)));

                // Check if `normal_index` is zero or positive. negative = no normal data
                //if (idx.normal_index >= 0) {
                //    tinyobj::real_t nx = attrib.normals[3 * size_t(idx.normal_index) + 0];
                //    tinyobj::real_t ny = attrib.normals[3 * size_t(idx.normal_index) + 1];
                //    tinyobj::real_t nz = attrib.normals[3 * size_t(idx.normal_index) + 2];
                //}

                // Check if `texcoord_index` is zero or positive. negative = no texcoord data
                //if (idx.texcoord_index >= 0) {
                //    tinyobj::real_t tx = attrib.texcoords[2 * size_t(idx.texcoord_index) + 0];
                //    tinyobj::real_t ty = attrib.texcoords[2 * size_t(idx.texcoord_index) + 1];
                //}

                // Optional: vertex colors
                //tinyobj::real_t red   = attrib.colors[3*size_t(idx.vertex_index)+0];
                //tinyobj::real_t green = attrib.colors[3*size_t(idx.vertex_index)+1];
                //tinyobj::real_t blue  = attrib.colors[3*size_t(idx.vertex_index)+2];
                //std::cout << red << ", " << green << ", " << blue << std::endl;
            }
            index_offset += fv;

            // per-face material
            //shapes[s].mesh.material_ids[f];
            //materials[shapes[s].mesh.material_ids[f]].

            cudaAddDiffuseTriangle(d_world, vertices[0], vertices[1], vertices[2], vec3(0.5, 0.5, 0.5));
        }
    }
}

/*
Sphere *sphere = new Sphere(Point(0.5f, 0.0f, 0.22f), 0.1f, Color(1, 0, 0));
    _scene.addShape(sphere);
    Sphere *sphere2 = new Sphere(Point(0.0f, 0.0f, 0.0f), 0.1f, Color(0, 1, 0));
    _scene.addShape(sphere2);
    Sphere *sphere3 = new Sphere(Point(-0.5f, 0.0f, -0.22f), 0.1f, Color(0, 0, 1));
    _scene.addShape(sphere3);
    //Sphere* sphere4 = new Sphere(Point(0.0f, 0.5f, 0.0f), 0.1f, Color(1, 0, 1));
    //_scene.addShape(sphere4);
    Plane* plane = new Plane(Point(0, 0.5, 0), Vector3(0, -1, 0), Color(0.2, 0.2, 0.2));
    _scene.addShape(plane);

    Triangle* triangle = new Triangle(Point(0.5f, 0.0f, 0.0f), Point(0.0f, 1.0f, 1.0f), Point(-0.5f, 0.0f, 1.0f), Color(0, 1, 1));
    _scene.addShape(triangle);
*/